# [这是一级标题](index.html)
## 这是二级标题
### 这是三级标题
#### 这是四级标题
##### 这是五级标题
###### 这是六级标题

一级标题
======================
二级标题
---------------------

[TOC]

> hello world!
> hello world!
> 标记之外`hello world`标记之外

> aaaaaaaaa
>> bbbbbbbbb
>>> cccccccccc

[百度](http://www.baidu.com/" 百度一下"){:target="_blank"}   

[![](./01.png '百度')](http://www.baidu.com){:target="_blank"} 

[![](./youku2.png)](http://v.youku.com/v_show/id_XMjgzNzM0NTYxNg==.html?spm=a2htv.20009910.contentHolderUnit2.A&from=y1.3-tv-grid-1007-9910.86804.1-2#paction){:target="_blank"}

|    a    |       b       |      c     |
|:-------:|:------------- | ----------:|
|   居中  |     左对齐    |   右对齐   |
|=========|===============|============|

Markdown[^1]
[^1]: Markdown是一种纯文本标记语言 
***
- **流程图**

```

graph TD
    A[开始]-->B(继续)
    B-->C{想一下}
    C-->|One| D[1]
    C-->|Two| E[2]
    C-->|Three| F[car]
    

```
***
 -表格
```

flow                            
st=>start: 启动|past:>http://www.baidu.com[blank]
e=>end: 结束                      
op1=>operation: 方案一             
sub2=>subroutine: 方案二|approved:>http://www.baidu.com[_parent] 
sub3=>subroutine: 重新制定方案        
cond1=>condition: 行不行？|request  
cond2=>condition: 行不行？         
io=>inputoutput: 结果满意          
st->op1->cond1
cond1(no,right)->sub2->cond2(no,right)->sub3(right)->op1
cond1(yes)->io->e      
cond2(yes)->io->e       //
```



